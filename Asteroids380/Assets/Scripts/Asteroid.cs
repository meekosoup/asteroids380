using System.Collections.Generic;
using UnityEngine;

public class Asteroid : MonoBehaviour
{
    public float speed = 5f;
    public List<GameObject> asteroidPrefabs = new List<GameObject>();

    private Rigidbody _rb;

    private void Awake()
    {
        _rb = GetComponent<Rigidbody>();
        RecalculateVelocity();
    }

    private void OnTriggerEnter(Collider other)
    {
        Laser laser = other.GetComponent<Laser>();
        if (laser)
        {
            laser.Remove();
            BreakAsteroid();
        }
    }

    private void BreakAsteroid()
    {
        foreach (var asteroidPrefab in asteroidPrefabs)
        {
            GameObject asteroid = Instantiate(asteroidPrefab, transform.position, Quaternion.identity);
        }
        
        Destroy(gameObject);
    }

    public void RecalculateVelocity()
    {
        Vector3 startingVelocity = new Vector3();
        startingVelocity.x = Random.Range(-speed, speed);
        startingVelocity.y = Random.Range(-speed, speed);
        _rb.velocity = startingVelocity;
    }
}
