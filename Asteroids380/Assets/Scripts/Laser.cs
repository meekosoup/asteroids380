using System;
using UnityEngine;

public class Laser : MonoBehaviour
{
    public enum Alliance
    {
        Player,
        Enemy,
    }

    public Alliance alliance = Alliance.Player;
    public float speed = 10f;
    public float lifeTime = 1f;

    private float _lifeElapsed;

    private void Awake()
    {
        _lifeElapsed = lifeTime;
    }

    private void Update()
    {
        _lifeElapsed -= Time.deltaTime;
        if (_lifeElapsed <= 0)
            Destroy(gameObject);
        
        transform.Translate(transform.up * speed * Time.deltaTime, Space.World);
    }

    public void Remove()
    {
        _lifeElapsed = 0f;
    }
}
