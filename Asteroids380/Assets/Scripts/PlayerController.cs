using UnityEngine;
using UnityEngine.InputSystem;

public class PlayerController : MonoBehaviour
{
    public float speed = 5f;
    public float rotationSpeed = 80f;
    public GameObject laserPrefab;
    public Transform firepoint;

    private PlayerInputActions _playerInputActions;
    private bool _moveInputDown = false;
    private Vector2 _movementInput;
    private Rigidbody _rb;

    private void Awake()
    {
        _rb = GetComponent<Rigidbody>();
        _playerInputActions = new PlayerInputActions();
        _playerInputActions.Enable();
    }

    private void OnEnable()
    {
        _playerInputActions.Player.Movement.performed += UpdateMovement;
        _playerInputActions.Player.Fire.performed += Fire;
    }

    private void OnDisable()
    {
        _playerInputActions.Player.Movement.performed -= UpdateMovement;
        _playerInputActions.Player.Fire.performed -= Fire;
    }

    private void Update()
    {
        if (_moveInputDown)
            Movement();
    }

    private void Movement()
    {
        // uses the vertical portion of the input to rotate the object on the z-axis
        transform.Rotate(_movementInput.x * -Vector3.forward * rotationSpeed * Time.deltaTime, Space.World);
        // uses the horizontal portion of the input to send the player forward (y-axis)
        // transform.Translate(_movementInput * Vector2.up * speed * Time.deltaTime);
        
        _rb.AddForce(_movementInput.y * transform.up * speed * Time.deltaTime, ForceMode.Impulse);
        
    }

    private void UpdateMovement(InputAction.CallbackContext context)
    {
        if (!context.performed)
        {
            _movementInput = Vector2.zero;
            _moveInputDown = false;
            return;
        }
        
        _movementInput = _playerInputActions.Player.Movement.ReadValue<Vector2>();
        _moveInputDown = true;
    }

    private void Fire(InputAction.CallbackContext context)
    {
        var laser = Instantiate(laserPrefab, firepoint.position, firepoint.rotation);
    }
}
