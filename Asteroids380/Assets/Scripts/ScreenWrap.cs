using System;
using UnityEngine;
using UnityEngine.InputSystem;

public class ScreenWrap : MonoBehaviour
{
    public Transform topBound;
    public Transform bottomBound;
    public Transform leftBound;
    public Transform rightBound;
    private Vector3 _pos;

    private void Awake()
    {
        if (topBound == null)
            topBound = GameObject.Find("topBound").transform;
        if (bottomBound == null)
            bottomBound = GameObject.Find("bottomBound").transform;
        if (leftBound == null)
            leftBound = GameObject.Find("leftBound").transform;
        if (rightBound == null)
            rightBound = GameObject.Find("rightBound").transform;
    }

    private void Update()
    {
        CheckScreenWrap();
    }

    private void CheckScreenWrap()
    {
        _pos = transform.position;

        if (transform.position.x < leftBound.position.x)
            _pos.x = rightBound.position.x;
        if (transform.position.x > rightBound.position.x)
            _pos.x = leftBound.position.x;
        if (transform.position.y > topBound.position.y)
            _pos.y = bottomBound.position.y;
        if (transform.position.y < bottomBound.position.y)
            _pos.y = topBound.position.y;

        transform.position = _pos;
    }

    
}
